const csv = require("../util");
// import the fs module
const fs = require("fs");
// path of the csv file
const path = "../data/matches.csv";
function matchesPerYear(path) {
  try {
  csv(path).then((data) => {
    // initialize the variable to get the output
    let matches = {};
    // to get the data from the array
    for (let index = 0; index < data.length; index++) {
      // store the elements in the element
      const element = data[index];
      // cheeck the data present or not in the variable
      if (!matches[element.season]) {
        matches[element.season] = 0;
      }
      matches[element.season] += 1;
    }
    // export the output into the json file
    fs.writeFile(
      "../public/output/matchesPerYear.json",
      JSON.stringify(matches),
      (err) => {
        if (err) {
          console.log(err);
        } else {
          console.log("Data updated Successfully... ");
        }
      }
    );
    //console.log(matches)
  });
}catch(err)
{
  console.log(err)
}
}
matchesPerYear(path);
