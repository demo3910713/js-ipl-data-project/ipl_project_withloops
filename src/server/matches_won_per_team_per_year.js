const csv = require("../util");
// import the fs module
const fs = require("fs");
// path of the csv file
const path = "../data/matches.csv";
function matchesWonPerYear(path) {
  try{
  csv(path).then((data) => {
    // initialize the variable to get the output
    let matchesWon = {};
    for (let index = 0; index < data.length; index++) {
      const element = data[index];
      if (element.winner == "") {
        element["winner"] = "NoResult";
      }
      if (!matchesWon[element.season]) {
        matchesWon[element.season] = {};
        if (!matchesWon[element.season][element.winner]) {
          matchesWon[element.season][element.winner] = 1;
        } else {
          matchesWon[element.season][element.winner] += 1;
        }
      } else {
        if (!matchesWon[element.season][element.winner]) {
          matchesWon[element.season][element.winner] = 1;
        } else {
          matchesWon[element.season][element.winner] += 1;
        }
      }
    }
    // store the output into the json file
    fs.writeFile(
      "../public/output/matchesWonPerTeamPerYear.json",
      JSON.stringify(matchesWon),
      (err) => {
        if (err) {
          console.log(err);
        } else {
          console.log("Data updated Successfully... ");
        }
      }
    );
  });
}catch(err)
{
  console.log(err)
}
}
matchesWonPerYear(path);
