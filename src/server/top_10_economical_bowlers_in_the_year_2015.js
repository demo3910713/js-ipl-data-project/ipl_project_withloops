const csv = require("../util");
// import the fs module
const fs = require("fs");
// path of the csv file
const path = "../data/matches.csv";
const path1 = "../data/deliveries.csv";
function top10EconomicalBolwersIn2015(path, path1) {
  try{
  csv(path).then((matchesData) => {
    csv(path1).then((deliveriesData) => {
      let bowlerRuns = {};
      let bowlerBalls = {};
      for (let index = 0; index < matchesData.length; index++) {
        const element = matchesData[index];
        // get the ids from the matches data
        if (element.season == 2015) {
          const id = element.id;
          for (let index = 0; index < deliveriesData.length; index++) {
            const deliveriesDetails = deliveriesData[index];

            if (id == deliveriesDetails.match_id) {
              if (!bowlerBalls[deliveriesDetails.bowler]) {
                bowlerBalls[deliveriesDetails.bowler] = 0;
                bowlerRuns[deliveriesDetails.bowler] = 0;
              }
              bowlerBalls[deliveriesDetails.bowler] += 1;
              bowlerRuns[deliveriesDetails.bowler] += Number(
                deliveriesDetails.total_runs
              );
            }
          }
        }
      }
      let bowlerEconomy = [];
      for (const key in bowlerBalls) {
        let temp = {};
        temp["name"] = key;
        temp["economy"] = bowlerRuns[key] / (bowlerBalls[key] / 6);
        bowlerEconomy.push(temp);
      }

      for (let index = 0; index < bowlerEconomy.length; index++) {
        for (let index1 = index + 1; index1 < bowlerEconomy.length; index1++) {
          const economy1 = bowlerEconomy[index].economy;
          const economy2 = bowlerEconomy[index1].economy;
          if (economy1 > economy2) {
            const temp = bowlerEconomy[index];
            bowlerEconomy[index] = bowlerEconomy[index1];
            bowlerEconomy[index1] = temp;
          }
        }
      }
      let result = bowlerEconomy.splice(0, 10);
      console.log(result);
      fs.writeFile(
        "../public/output/top!10EconomicalBowlers2015.json",
        JSON.stringify(result),
        (err) => {
          if (err) {
            console.log(err);
          } else {
            console.log("Data updated Successfully... ");
          }
        }
      );
    });
  });
} catch (error) {
 console.log(error)   
}
}
top10EconomicalBolwersIn2015(path, path1);
