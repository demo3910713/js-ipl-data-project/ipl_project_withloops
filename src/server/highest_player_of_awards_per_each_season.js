const csv = require("../util");
// import the fs module
const fs = require("fs");
// path of the csv file
const path = "../data/matches.csv";
function highestPlayerOfTheAwardsPerEachSeason(path) {
  try {
  csv(path).then((data) => {
    let playerOfTHeAwardsPerSeason = {};
    for (let index = 0; index < data.length; index++) {
      const element = data[index];
      if (!playerOfTHeAwardsPerSeason[element.season]) {
        playerOfTHeAwardsPerSeason[element.season] = {};
        if (
          !playerOfTHeAwardsPerSeason[element.season][element.player_of_match]
        ) {
          playerOfTHeAwardsPerSeason[element.season][
            element.player_of_match
          ] = 1;
        } else {
          playerOfTHeAwardsPerSeason[element.season][
            element.player_of_match
          ] += 1;
        }
      } else {
        if (
          !playerOfTHeAwardsPerSeason[element.season][element.player_of_match]
        ) {
          playerOfTHeAwardsPerSeason[element.season][
            element.player_of_match
          ] = 1;
        } else {
          playerOfTHeAwardsPerSeason[element.season][
            element.player_of_match
          ] += 1;
        }
      }
    }
    let highestPOTMWinners = {};
    for (const season in playerOfTHeAwardsPerSeason) {
      for (const player in playerOfTHeAwardsPerSeason[season]) {
        const awards = playerOfTHeAwardsPerSeason[season][player];
        if (
          !highestPOTMWinners[season] ||
          awards > highestPOTMWinners[season].awards
        ) {
          highestPOTMWinners[season] = { player, awards };
        }
      }
    }
    //console.log(highestPOTMWinners)
    fs.writeFile(
      "../public/output/highestPlayerOfAwardsPerEachSeason.json",
      JSON.stringify(highestPOTMWinners),
      (err) => {
        if (err) {
          console.log(err);
        } else {
          console.log("Data updated Successfully... ");
        }
      }
    );
  });
} catch (error) {
   console.log(error) 
}
}
highestPlayerOfTheAwardsPerEachSeason(path);
