const csv = require("../util");
// import the fs module
const fs = require("fs");
// path of the csv file
const path = "../data/deliveries.csv";
function highestNoOfTimesDimissedByAnotherPlayer(path, batsman) {
  try {
  csv(path).then((data) => {
    let dismissalData = {};
    for (let index = 0; index < data.length; index++) {
      let element = data[index];
      if (element.player_dismissed === batsman) {
        if (!dismissalData[element.bowler]) {
          dismissalData[element.bowler] = 0;
        }
        dismissalData[element.bowler] += 1;
      }
    }
    const entries = Object.entries(dismissalData);

    for (let i = 0; i < entries.length; i++) {
      for (let j = i + 1; j < entries.length; j++) {
        if (entries[i][1] < entries[j][1]) {
          const temp = entries[i];
          entries[i] = entries[j];
          entries[j] = temp;
        }
      }
    }
    fs.writeFile(
      "../public/output/highestNoOfDimissedByAnotherPlayer.json",
      JSON.stringify(entries[0]),
      (err) => {
        if (err) {
          console.log(err);
        } else {
          console.log("Data updated Successfully... ");
        }
      }
    );
  });
} catch (error) {
    console.log(error)
}
}
highestNoOfTimesDimissedByAnotherPlayer(path, "SR Watson");
