const csv = require("../util");
// import the fs module
const fs = require("fs");
// path of the csv file
const path = "../data/matches.csv";
const path1 = "../data/deliveries.csv";
function noOfTimesWonTossAndMatch(path) {
  try {
  csv(path).then((matchesData) => {
    let result = {};
    for (let index = 0; index < matchesData.length; index++) {
      const element = matchesData[index];
      if (element.toss_winner == element.winner) {
        if (!result[element.winner]) {
          result[element.winner] = 0;
        }
        result[element.winner] += 1;
      }
    }
    fs.writeFile(
      "../public/output/noOfTimesWonTossAndMatch.json",
      JSON.stringify(result),
      (err) => {
        if (err) {
          console.log(err);
        } else {
          console.log("Data updated Successfully... ");
        }
      }
    );
  });
} catch (error) {
 console.log(error)   
}
}
noOfTimesWonTossAndMatch(path);
